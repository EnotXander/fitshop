<?php
/**
 * JBZoo App is universal Joomla CCK, application for YooTheme Zoo component
 * @package     jbzoo
 * @version     2.x Pro
 * @author      JBZoo App http://jbzoo.com
 * @copyright   Copyright (C) JBZoo.com,  All rights reserved.
 * @coder       Denis Smetannikov <denis@jbzoo.com>
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


/**
 * Class JBModelModelCategorySearch
 */
class JBModelModelCategorySearch extends JBModel
{

    const LIMIT_CATEGORIES = 200;
    const LIMIT_ITEMS      = 3000;

    /**
     * Create and return self instance
     * @return JBModelModelCategorySearch
     */
    public static function model()
    {
        return new self();
    }

    /**
     * @param $text
     * @param $phrase
     * @param $curCat
     * @return mixed
     */
    public function loadCategories($text, $phrase, $curCat)
    {
        $words = $this->_morphTrimmer($text);
        if (empty($words)) {
            return array();
        }

        $select = $this->_getSelect()
            ->select('DISTINCT tCategory.id')
            ->from(ZOO_TABLE_ITEM, 'tItem')
            ->leftJoin(ZOO_TABLE_CATEGORY_ITEM . ' AS tCategoryItem ON tItem.id = tCategoryItem.item_id')
            ->leftJoin(ZOO_TABLE_CATEGORY . ' AS tCategory ON tCategoryItem.category_id = tCategory.id')
            ->where('tItem.' . $this->app->user->getDBAccessString())
            ->where('tItem.state = ?', 1)
            ->where('(tItem.publish_up = ' . $this->_dbNull . ' OR tItem.publish_up <= ' . $this->_dbNow . ')')
            ->where('(tItem.publish_down = ' . $this->_dbNull . ' OR tItem.publish_down >= ' . $this->_dbNow . ')')
            ->limit(self::LIMIT_CATEGORIES);

        if ($curCat) {
            $catList = JBModelCategory::model()->getNestedCategories($curCat);
            $catList[] = $curCat;
            $catList = array_filter($catList);
            if (!empty($catList)) {
                $select->where('tCategoryItem.category_id IN (' . implode(',', $catList) . ')');
            } else {
                $select->where('tCategory.id > ?', 0);
            }
        } else {
            $select->where('tCategory.id > ?', 0);
        }

        $wheres = array();
        foreach ($words as $word) {
            $word = $this->_db->Quote('%' . $this->_db->escape($word, true) . '%', false);

            $like = array(
                'tItem.name LIKE ' . $word,

                'EXISTS (SELECT value FROM ' . ZOO_TABLE_SEARCH
                . ' WHERE tItem.id = item_id AND value LIKE ' . $word . ' AND ' . $this->_getAccessExclude() . ')',

                'EXISTS (SELECT name FROM ' . ZOO_TABLE_TAG . ' WHERE tItem.id = item_id AND name LIKE ' . $word . ')',
            );

            $wheres[] = implode(' OR ', $like);
        }

        $where = '(' . implode(') OR (', $wheres) . ')';
        $select->where('(' . $where . ')');

        return $this->fetchList($select);
    }

    /**
     * @param $text
     * @param $phrase
     * @param $curCat
     * @return mixed
     */
    public function loadItems($text, $phrase, $curCat)
    {
        $places = array('name', 'tag', 'index', 'category');
        $words  = $this->_morphTrimmer($text);
        if (empty($words)) {
            return array();
        }

        $selects = array();
        foreach ($places as $place) {

            foreach ($words as $word) {
                $word = trim($word);
                if (!$word) {
                    continue;
                }

                $word = $this->_db->Quote('%' . $this->_db->escape($word, true) . '%', false);

                $select = $this->_getItemSubSelect();
                if ($place == 'name') {
                    $select->where('tItem.name LIKE ' . $word);

                } else if ($place == 'tag') {

                    $subSelect = $this->_getSelect()
                        ->select('name')
                        ->from(ZOO_TABLE_TAG . ' AS tTag')
                        ->where('tItem.id = tTag.item_id')
                        ->where('tTag.name LIKE ' . $word);

                    $select->where('EXISTS (' . $subSelect . ')');

                } else if ($place == 'category') {

                    $subSelect = $this->_getSelect()
                        ->select('name')
                        ->from(ZOO_TABLE_CATEGORY . ' AS tCategory')
                        ->leftJoin(ZOO_TABLE_CATEGORY_ITEM . ' AS tCategoryItem ON tCategory.id = tCategoryItem.category_id')
                        ->where('tItem.id = tCategoryItem.item_id')
                        ->where('tCategory.name LIKE ' . $word);

                    $select->where('EXISTS (' . $subSelect . ')');

                } else if ($place == 'index') {

                    $subSelect = $this->_getSelect()
                        ->select('value')
                        ->from(ZOO_TABLE_SEARCH . ' AS tIndex')
                        ->where('tItem.id = tIndex.item_id')
                        ->where('tIndex.value LIKE ' . $word)
                        ->where($this->_getAccessExclude());

                    $select->where('EXISTS (' . $subSelect . ')');
                }

                $selects[] = $select->__toString();
            }
        }

        $union = '(' . implode(') UNION ALL (', $selects) . ')';

        $allSelect = $this->_getSelect()
            ->select('tAll.id')
            ->select('COUNT(tAll.id) AS count')
            ->from('(' . $union . ') AS tAll')
            ->group('tAll.id');

        $main = $this->_getSelect()
            ->select(array('tWrap.id', 'tWrap.count', 'tCategoryItem.category_id'))
            ->from('(' . $allSelect . ') AS tWrap')
            ->leftJoin(ZOO_TABLE_CATEGORY_ITEM . ' AS tCategoryItem ON tWrap.id = tCategoryItem.item_id')
            ->where('tCategoryItem.category_id > ?', 0)
            ->order('tWrap.count DESC')
            ->where('tWrap.count >= ?', count($words));

        if ($curCat) {
            $catList = JBModelCategory::model()->getNestedCategories($curCat);            
            $catList[] = $curCat;
            $catList = array_filter($catList);
            if (!empty($catList)) {            
                $main->where('tCategoryItem.category_id IN (' . implode(',', $catList) . ')');
            }
        }

        return $this->fetchAll($main);
    }

    /**
     * @return JBDatabaseQuery
     */
    private function _getItemSubSelect()
    {
        $select = $this->_getSelect()
            ->select('DISTINCT tItem.id')
            ->from(ZOO_TABLE_ITEM, 'tItem')
            ->where('tItem.' . $this->app->user->getDBAccessString())
            ->where('tItem.state = ?', 1)
            ->where('(tItem.publish_up = ' . $this->_dbNull . ' OR tItem.publish_up <= ' . $this->_dbNow . ')')
            ->where('(tItem.publish_down = ' . $this->_dbNull . ' OR tItem.publish_down >= ' . $this->_dbNow . ')')
            ->limit(self::LIMIT_ITEMS);

        return $select;
    }

    /**
     * @return string
     */
    protected function _getAccessExclude()
    {
        $elements = array();

        /** @var Application $application */
        foreach ($this->app->application->groups() as $application) {

            /** @var Type $type */
            foreach ($application->getTypes() as $type) {

                /** @var Element $element */
                foreach ($type->getElements() as $element) {
                    if (!$element->canAccess()) {
                        $elements[] = $this->_db->Quote($element->identifier);
                    }
                }
            }
        }

        $access = $elements ? 'NOT element_id in (' . implode(',', $elements) . ')' : '1';

        return $access;
    }

    /**
     * @param $idList
     * @return array
     */
    public function createItems($idList)
    {
        $items = $this->getZooItemsByIds($idList);
        $items = $this->app->jbarray->sortByArray($items, $idList);
        return $items;
    }


    /**
     * @param $word
     * @return mixed
     */
    protected function _dropBackWords($word)
    {
        $reg  = "/(ый|ой|ая|ое|ые|ому|а|о|у|е|ого|ему|и|ство|ых|ох|ия|ий|ь|я|он|ют|ат|ы)$/iu";
        $word = preg_replace($reg, '', $word);
        return $word;
    }

    /**
     * @param $query
     * @return mixed
     */
    protected function _stopWords($query)
    {
        $reg   = "/\s(под|много|что|когда|где|или|поэтому|все|будем|как)\s/iu";
        $query = preg_replace($reg, ' ', ' ' . $query . ' ');
        $query = JString::trim($query);
        return $query;
    }

    /**
     * @param $query
     * @return array
     */
    protected function _morphTrimmer($query)
    {
        $words = explode(" ", $this->_stopWords($query));

        $keywords = array();
        foreach ($words as $word) {

            $word = JString::trim($word);

            if (JString::strlen($word) < 3) {
                continue;
            } else {
                $keywords[] = $this->_dropBackWords($word);
            }
        }

        return $keywords;
    }

}