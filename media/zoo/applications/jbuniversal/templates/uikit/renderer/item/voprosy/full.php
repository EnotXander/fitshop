﻿<?php
/**
 * JBZoo App is universal Joomla CCK, application for YooTheme Zoo component
 *
 * @package     jbzoo
 * @version     2.x Pro
 * @author      JBZoo App http://jbzoo.com
 * @copyright   Copyright (C) JBZoo.com,  All rights reserved.
 * @license     http://jbzoo.com/license-pro.php JBZoo Licence
 * @coder       Denis Smetannikov <denis@jbzoo.com>
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$align  = $this->app->jbitem->getMediaAlign($item, $layout);
$tabsId = $this->app->jbstring->getId('tabs');

?>

<div class="uk-panel uk-panel-box uk-clearfix">
    <?php if ($this->checkPosition('title')) : ?>
        <h6 class="item-title"><?php echo $this->renderPosition('title'); ?></h6>
    <?php endif; ?>

    <div class="uk-grid">
        <?php if ($this->checkPosition('image')) : ?>
            <div class="uk-width-medium-1-2">
                <div class="item-image">
                    <?php echo $this->renderPosition('image'); ?>
                </div>
            </div>
        <?php endif; ?>
</div>
</div>
<br>

	<a class="btn btn-primary" target="_blank">Ответ</a>	
<p>
<div class="uk-panel uk-panel-box uk-clearfix">		
        <?php if ($this->checkPosition('meta')) : ?>
            <div class="uk-width-medium-1">
                <div class="item-metadata">
                    <ul class="uk-list">

                        <?php echo $this->renderPosition('meta', array('style' => 'list')); ?>

                    </ul>
                </div>
            </div>
        <?php endif; ?>
    </div>


</div>
