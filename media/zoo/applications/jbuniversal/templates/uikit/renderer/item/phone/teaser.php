<?php
/**
 * JBZoo App is universal Joomla CCK, application for YooTheme Zoo component
 * @package     jbzoo
 * @version     2.x Pro
 * @author      JBZoo App http://jbzoo.com
 * @copyright   Copyright (C) JBZoo.com,  All rights reserved.
 * @license     http://jbzoo.com/license-pro.php JBZoo Licence
 * @coder       Denis Smetannikov <denis@jbzoo.com>
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>

<div class="jbzoo-teaser-shadow">
    <div class="jbzoo-teaser-wrapper">
        <?php if ($this->checkPosition('title')) { ?>            
		<div class="jbzoo-item-title">                
		<?php echo $this->renderPosition('title'); ?>
		</div>        
		<?php 
		}
		if ($this->checkPosition('image')) { ?>
            <div class="uk-clearfix jbzoo-no-border jbzoo-image uk-text-center">
                <?php echo $this->renderPosition('image'); ?>
            </div>
        <?php }

      

        if ($this->checkPosition('price') || $this->checkPosition('rating')) { ?>
            <div class="jbzoo-info uk-clearfix uk-margin-small">
                <div class="jbzoo-info-price uk-float-left uk-width-small-1-2">
                    <?php echo $this->renderPosition('price'); ?>
                </div>

                <div class="jbzoo-info-rating uk-float-left uk-width-small-1-2">
                    <?php echo $this->renderPosition('rating'); ?>
                </div>
            </div>
        <?php }

        if ($this->checkPosition('tools-user') || $this->checkPosition('tools-buttons')) { ?>
            <div class="jbzoo-tools uk-clearfix uk-margin-small">

                <div class="jbzoo-tools-buttons uk-display-inline-block uk-width-small">
                    <?php echo $this->renderPosition('tools-buttons'); ?>
                </div>

                <?php echo $this->renderPosition('tools-user', array(
                        'style' => 'jbblock',
                        'class' => 'uk-display-inline-block uk-width-small-2-10')
                ); ?>

            </div>
        <?php }

        if ($this->checkPosition('properties')) {
            echo $this->renderPosition('properties', array('style' => 'list'));
        } ?>
    </div>
</div>