<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
App::getInstance('zoo')->loader->register('JBCSVItem', 'jbelements:item.php');

class JBCSVItemuserimagebox extends JBCSVItem {

    public function toCSV()
    {
        $result = array();
        foreach($this->_value['option'] as $option) {
            $result[] .= $option;
        }
        return implode(',', $result);
    }

    public function fromCSV($value, $position = null)
    {
        $options = explode(',' , $value);
        $this->_element->bindData(array('option' => $options));
        return $this->_item;
    }
}