<?php
/**
* @version   $Id: error.php 24195 2014-11-21 16:07:13Z james $
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );

// Load and Inititialize Gantry Class
global $gantry;
require_once(dirname(__FILE__) . '/lib/gantry/gantry.php');
$gantry->init();

// Load Mootools
JHTML::_('behavior.framework', true);

$doc = JFactory::getDocument();
$app = JFactory::getApplication();

// Less Variables
$lessVariables = array(
    'accent-color1'             => $gantry->get('accent-color1',                '#66BDBB'),
    'accent-color2'             => $gantry->get('accent-color2',                '#80C294'),
    'accent-color3'             => $gantry->get('accent-color3',                '#FEFCD0'),

    'sideslider-textcolor'      => $gantry->get('sideslider-textcolor',         '#7A7A7A'),
    'sideslider-background'     => $gantry->get('sideslider-background',        '#F6F6F6'),

    'header-textcolor'          => $gantry->get('header-textcolor',             '#333333'),
    'header-background'         => $gantry->get('header-background',            '#FFFFFF'),

    'showcase-textcolor'        => $gantry->get('showcase-textcolor',           '#7A7A7A'),
    'showcase-background'       => $gantry->get('showcase-background',          '#FFFFFF'),

    'feature-textcolor'         => $gantry->get('feature-textcolor',            '#FFFFFF'),
    'feature-background'        => $gantry->get('feature-background',           '#333333'),

    'utility-textcolor'         => $gantry->get('utility-textcolor',            '#888888'),
    'utility-background'        => $gantry->get('utility-background',           '#EEEEEE'),

    'expandedtop-textcolor'     => $gantry->get('expandedtop-textcolor',        '#7A7A7A'),
    'expandedtop-background'    => $gantry->get('expandedtop-background',       '#FFFFFF'),

    'maintop-textcolor'         => $gantry->get('maintop-textcolor',            '#FFFFFF'),
    'maintop-background'        => $gantry->get('maintop-background',           '#5BBCBB'),

    'mainbottom-textcolor'      => $gantry->get('mainbottom-textcolor',         '#FFFFFF'),
    'mainbottom-background'     => $gantry->get('mainbottom-background',        '#7BC094'),

    'additiontop-textcolor'     => $gantry->get('additiontop-textcolor',        '#383838'),
    'additiontop-background'    => $gantry->get('additiontop-background',       '#FFFFFF'),

    'extension-textcolor'       => $gantry->get('extension-textcolor',          '#888888'),
    'extension-background'      => $gantry->get('extension-background',         '#EEEEEE'),

    'additionbottom-textcolor'  => $gantry->get('additionbottom-textcolor',     '#383838'),
    'additionbottom-background' => $gantry->get('additionbottom-background',    '#FFFFFF'),

    'mainbody-overlay'          => $gantry->get('mainbody-overlay',             'light'),

    'bottom-textcolor'          => $gantry->get('bottom-textcolor',             '#7A7A7A'),
    'bottom-background'         => $gantry->get('bottom-background',            '#FFFFFF'),

    'footer-textcolor'          => $gantry->get('footer-textcolor',             '#888888'),
    'footer-background'         => $gantry->get('footer-background',            '#EEEEEE'),

    'copyright-textcolor'       => $gantry->get('copyright-textcolor',          '#888888'),
    'copyright-background'      => $gantry->get('copyright-background',         '#EEEEEE')
);

$gantry->addStyle('grid-responsive.css', 5);
$gantry->addLess('bootstrap.less', 'bootstrap.css', 6);
$gantry->addLess('error.less', 'error.css', 4, $lessVariables);

// Scripts
if ($gantry->browser->name == 'ie'){
	if ($gantry->browser->shortversion == 8){
		$gantry->addScript('html5shim.js');
		$gantry->addScript('placeholder-ie.js');
	}
	if ($gantry->browser->shortversion == 9){
		$gantry->addInlineScript("if (typeof RokMediaQueries !== 'undefined') window.addEvent('domready', function(){ RokMediaQueries._fireEvent(RokMediaQueries.getQuery()); });");
		$gantry->addScript('placeholder-ie.js');
	}
}
if ($gantry->get('layout-mode', 'responsive') == 'responsive') $gantry->addScript('rokmediaqueries.js');

ob_start();
?>
<body id="rt-error" <?php echo $gantry->displayBodyTag(); ?>>
	<div id="rt-page-surround">
		<div class="rt-pagesurround-overlay">
			<div id="rt-body-surround">

				<header id="rt-header-surround">
					<div class="rt-bg-overlay">
						<div id="rt-header">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('header','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<div id="rt-feature">
							<div class="rt-bg-overlay">
								<div class="rt-container">
									<div class="rt-flex-container">
										<div class="rt-error-body">
											<div class="rt-error-header">
												<div class="rt-error-code"><?php echo $this->error->getCode(); ?></div>
												<div class="rt-error-code-desc"><?php echo $this->error->getMessage(); ?></div>
											</div>
											<div class="rt-error-content">
												<div class="rt-error-title"><?php echo JText::_("RT_ERROR_TITLE"); ?></div>
												<div class="rt-error-message"><?php echo JText::_("RT_ERROR_MESSAGE"); ?></div>
												<div class="rt-error-button"><a href="<?php echo $gantry->baseUrl; ?>" class="readon"><span><?php echo JText::_("RT_ERROR_HOME"); ?></span></a></div>
											</div>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

				<footer id="rt-footer-surround">
					<div id="rt-copyright">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</footer>

			</div>
		</div>
	</div>
</body>
</html>
<?php

$body = ob_get_clean();
$gantry->finalize();

require_once(JPATH_LIBRARIES.'/joomla/document/html/renderer/head.php');
$header_renderer = new JDocumentRendererHead($doc);
$header_contents = $header_renderer->render(null);
ob_start();
?>
<!doctype html>
<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
	<?php echo $header_contents; ?>
	<?php if ($gantry->get('layout-mode') == '960fixed') : ?>
	<meta name="viewport" content="width=960px">
	<?php elseif ($gantry->get('layout-mode') == '1200fixed') : ?>
	<meta name="viewport" content="width=1200px">
	<?php else : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php endif; ?>
</head>
<?php
$header = ob_get_clean();
echo $header.$body;