<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


$cols     = 2;
$maxItems = 6;

$app         = App::getInstance('zoo');
$application = $app->zoo->getApplication();
$template    = $application->getTemplate();

/** @var JBLayoutHelper $jblayout */
$jblayout = $app->jblayout;

$this->params->set('template.item_cols', $cols);
$this->params->set('template.item_order', 0);
$this->application = $application;
$this->renderer    = $app->renderer->create('item')->addPath(array(
    $app->path->path('component.site:'),
    $template->getPath(),
));

$jblayout->setView($this);
$isCategory = plgSearchZooCategory::getCategory();
$currentUrl = $app->jbenv->getCurrentUrl();

?>


<?php /* if  (!$isCategory): ?> 
    <h2>Найдено в категориях</h2>
    <ul class="categpoisk" style="clear:both;">
        <?php
        $categories = array();
        foreach ($this->results as $category) {
            if (isset($category->categories)) {
                $categories = $category->categories;
                break;
            }
        }

        foreach ($categories as $category) :

            $url = $currentUrl . '#cat-' . $category->alias;

            if (isset($category->isEmpty)) : ?>
                <li class="sername btn btn-primary pull-left"><?php echo $category->treename; ?></li>
            <?php else: ?>
                <li class="serlink btn btn-primary pull-left"><a class="btn btn-primary pull-left" href="<?php echo $url; ?>"><?php echo $category->treename; ?></a></li>
            <?php endif; ?>

        <?php endforeach; ?>
    </ul>
<?php endif; */ ?> 




<?php if (!$isCategory): ?>
    <i class="clr">&nbsp;</i>
    <h2>Найденные товары</h2>
<?php endif; ?>

<div>
    <?php
    foreach ($this->results as $category) :

        $items = $category->items;
        $count = count($items);

        if (count($category->items) == 0) {
            continue;
        }

        if (!$isCategory) {
            if ($count > $maxItems) {
                $items = array_splice($items, 0, $maxItems);
            }
        }

        $items = plgSearchZooCategory::createItems($items);
        $url = plgSearchZooCategory::getCategoryLink($category->catId);

        ?>

        <?php if (!$isCategory) : ?>
            <a name="cat-<?php echo $category->alias; ?>"></a>
            <h3><?php echo($category->title ? $category->title : 'Вне категории'); ?></h3>
            <i>Найдено — <?php echo $count; ?>.
                <?php if ($url && $count > $maxItems) { ?>
                    <a href="<?php echo $url; ?>">Смотреть все</a>
                <?php } ?>
            </i>
        <?php endif; ?>

        <div class="items">
            <?php echo $jblayout->render('items', $items); ?>
        </div>

    <?php endforeach; ?>
</div>

<div class="pagination">
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>
