<?php
/**
 * @version   $Id: gantry.config.php 23647 2014-10-30 16:19:32Z arifin $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('JPATH_BASE') or die();

$gantry_presets = array(
    'presets' => array(
        'preset1' => array(
            'name'                        => 'Preset 1',

            'accent-color1'               => '#66BDBB',
            'accent-color2'               => '#80C294',
            'accent-color3'               => '#FEFCD0',

            'demostyle-type'              => 'preset1',

            'sideslider-textcolor'        => '#7A7A7A',
            'sideslider-background'       => '#F6F6F6',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#66BDBB',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#80C294',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset2' => array(
            'name'                        => 'Preset 2',

            'accent-color1'               => '#F46C8B',
            'accent-color2'               => '#70B5CB',
            'accent-color3'               => '#FCE884',

            'demostyle-type'              => 'preset2',

            'sideslider-textcolor'        => '#989898',
            'sideslider-background'       => '#1C1C1C',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#F46C8B',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#70B5CB',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset3' => array(
            'name'                        => 'Preset 3',

            'accent-color1'               => '#A95482',
            'accent-color2'               => '#63C5BE',
            'accent-color3'               => '#4A96BA',

            'demostyle-type'              => 'preset3',

            'sideslider-textcolor'        => '#74838B',
            'sideslider-background'       => '#1F2629',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#A95482',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#63C5BE',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset4' => array(
            'name'                        => 'Preset 4',

            'accent-color1'               => '#E66027',
            'accent-color2'               => '#F9AD18',
            'accent-color3'               => '#F88316',

            'demostyle-type'              => 'preset4',

            'sideslider-textcolor'        => '#616F76',
            'sideslider-background'       => '#1F2629',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#E66027',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#F9AD18',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset5' => array(
            'name'                        => 'Preset 5',

            'accent-color1'               => '#3173B8',
            'accent-color2'               => '#3AADB9',
            'accent-color3'               => '#38BC75',

            'demostyle-type'              => 'preset5',

            'sideslider-textcolor'        => '#7F97BC',
            'sideslider-background'       => '#1C3762',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#3173B8',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#3AADB9',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset6' => array(
            'name'                        => 'Preset 6',

            'accent-color1'               => '#2FAFA3',
            'accent-color2'               => '#111111',
            'accent-color3'               => '#CFE895',

            'demostyle-type'              => 'preset6',

            'sideslider-textcolor'        => '#7A7A7A',
            'sideslider-background'       => '#F6F6F6',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#2FAFA3',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#111111',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset7' => array(
            'name'                        => 'Preset 7',

            'accent-color1'               => '#F85F5B',
            'accent-color2'               => '#111111',
            'accent-color3'               => '#7ADCED',

            'demostyle-type'              => 'preset7',

            'sideslider-textcolor'        => '#7A7A7A',
            'sideslider-background'       => '#F6F6F6',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#F85F5B',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#80C294',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        ),

        'preset8' => array(
            'name'                        => 'Preset 8',

            'accent-color1'               => '#44CEDD',
            'accent-color2'               => '#111111',
            'accent-color3'               => '#FCF99E',

            'demostyle-type'              => 'preset8',

            'sideslider-textcolor'        => '#7A7A7A',
            'sideslider-background'       => '#F6F6F6',

            'top-textcolor'               => '#333333',
            'top-background'              => '#FFFFFF',

            'header-textcolor'            => '#333333',
            'header-background'           => '#FFFFFF',

            'showcase-textcolor'          => '#7A7A7A',
            'showcase-background'         => '#FFFFFF',

            'feature-textcolor'           => '#FFFFFF',
            'feature-background'          => '#333333',

            'utility-textcolor'           => '#888888',
            'utility-background'          => '#EEEEEE',

            'expandedtop-textcolor'       => '#7A7A7A',
            'expandedtop-background'      => '#FFFFFF',

            'maintop-textcolor'           => '#FFFFFF',
            'maintop-background'          => '#44CEDD',

            'expandedbottom-textcolor'    => '#7A7A7A',
            'expandedbottom-background'   => '#FFFFFF',

            'mainbottom-textcolor'        => '#FFFFFF',
            'mainbottom-background'       => '#111111',

            'additiontop-textcolor'       => '#383838',
            'additiontop-background'      => '#FFFFFF',

            'extension-textcolor'         => '#888888',
            'extension-background'        => '#EEEEEE',

            'additionbottom-textcolor'    => '#383838',
            'additionbottom-background'   => '#FFFFFF',

            'bottom-textcolor'            => '#7A7A7A',
            'bottom-background'           => '#FFFFFF',

            'footer-textcolor'            => '#888888',
            'footer-background'           => '#EEEEEE',

            'copyright-textcolor'         => '#888888',
            'copyright-background'        => '#EEEEEE'

        )
    )
);
