<?php
/**
 * @version   $Id: index.php 23716 2014-11-03 10:59:12Z arifin $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */

/* No Direct Access */
defined( '_JEXEC' ) or die( 'Restricted index access' );
/* Load Mootools */
JHTML::_('behavior.framework', true);
/* Load and Inititialize Gantry Class */
require_once(dirname(__FILE__) . '/lib/gantry/gantry.php');
$gantry->init();
/* Get the Current Preset */
$gpreset = str_replace(' ','',strtolower($gantry->get('name')));
?>
<!doctype html>
<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
<?php if ($gantry->get('layout-mode') == '960fixed') : ?>
	<meta name="viewport" content="width=960px">
<?php elseif ($gantry->get('layout-mode') == '1200fixed') : ?>
	<meta name="viewport" content="width=1200px">
<?php else : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php endif; ?>
<?php /* Head */
	$gantry->displayHead();
?>
<?php /* Force IE to Use the Most Recent Version */ if ($gantry->browser->name == 'ie') : ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php endif; ?>

<?php
	$gantry->addLess('bootstrap.less', 'bootstrap.css', 6);
	if ($gantry->browser->name == 'ie'){
		if ($gantry->browser->shortversion == 8){
			$gantry->addScript('html5shim.js');
			$gantry->addScript('canvas-unsupported.js');
			$gantry->addScript('placeholder-ie.js');
		}
		if ($gantry->browser->shortversion == 9){
			$gantry->addInlineScript("if (typeof RokMediaQueries !== 'undefined') window.addEvent('domready', function(){ RokMediaQueries._fireEvent(RokMediaQueries.getQuery()); });");
			$gantry->addScript('placeholder-ie.js');
		}
	}
	if ($gantry->get('layout-mode', 'responsive') == 'responsive') $gantry->addScript('rokmediaqueries.js');

    // SideSlider
    global $ie_ver;
    if ($gantry->browser->name == 'ie' && $gantry->browser->shortversion == 8) $ie_ver = 'ie8';

    if ($ie_ver != 'ie8' && $gantry->countModules('sideslider')) {
        $gantry->addScript('classie.js');
        $gantry->addScript('sideslider.js');
    }

?>
</head>
<body <?php echo $gantry->displayBodyTag(); ?> id="<?php echo ($gantry->countModules('sideslider') ? 'with':'without') ?>-sideslider">


	<div id="rt-page-surround">
		<?php /** Begin SideSlider **/ if ($gantry->get('sideslider-type')=='static' and $gantry->countModules('sideslider')) : ?>
		<div id="rt-sideslider">
			<div class="rt-sideslider-wrap">
				<div class="rt-sideslider">
					<?php echo $gantry->displayModules('sideslider','basic','standard'); ?>
				</div>
				<button class="rt-sideslider-close-button" id="rt-sideslider-close-button"></button>
			</div>
			<button class="rt-sideslider-button" id="rt-sideslider-open-button"></button>
		</div>
		<?php /** End SideSlider **/ endif; ?>
		<div id="rt-body-surround">
			<?php /** Begin SideSlider **/ if ($gantry->get('sideslider-type')=='dynamic' and $gantry->countModules('sideslider')) : ?>
			<div id="rt-sideslider">
				<div class="rt-sideslider-wrap">
					<div class="rt-sideslider">
						<?php echo $gantry->displayModules('sideslider','basic','standard'); ?>
					</div>
					<button class="rt-sideslider-close-button" id="rt-sideslider-close-button"></button>
				</div>
				<button class="rt-sideslider-button" id="rt-sideslider-open-button"></button>
			</div>
			<?php /** End SideSlider **/ endif; ?>

			<div id="rt-sideslider-overlay">

				<?php /** Begin Header Surround **/ if ($gantry->countModules('drawer') or $gantry->countModules('top') or $gantry->countModules('header') or $gantry->countModules('showcase') or $gantry->countModules('feature')) : ?>
				<header id="rt-header-surround">
					<div class="rt-bg-overlay">
						<?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
						<div id="rt-drawer">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('drawer','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<?php /** End Drawer **/ endif; ?>
						<?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
						<div id="rt-top">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('top','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<?php /** End Top **/ endif; ?>
						<?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
						<div id="rt-header">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('header','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<?php /** End Header **/ endif; ?>
						<?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
						<div id="rt-showcase">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('showcase','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<?php /** End Showcase **/ endif; ?>
						<?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
						<div id="rt-feature">
							<div class="rt-bg-overlay">
								<div class="rt-container">
									<div class="rt-flex-container">
										<?php echo $gantry->displayModules('feature','standard','standard'); ?>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
						<?php /** End Feature **/ endif; ?>
					</div>
				</header>
				<?php /** End Header Surround **/ endif; ?>

				<?php /** Begin FullWidthTop **/ if ($gantry->countModules('fullwidthtop')) : ?>
				<div id="rt-fullwidthtop">
					<?php echo $gantry->displayModules('fullwidthtop','basic','standard'); ?>
					<div class="clear"></div>
				</div>
				<?php /** End FullWidthTop **/ endif; ?>

				<?php /** Begin Main Section **/ if ($gantry->countModules('breadcrumb') or $gantry->countModules('utility') or $gantry->countModules('expandedtop') or $gantry->countModules('maintop') or $gantry->countModules('expandedbottom') or $gantry->countModules('mainbottom') or $gantry->countModules('additiontop') or $gantry->countModules('extension') or $gantry->countModules('additionbottom')) : ?>
				<section id="rt-main-surround">
					<?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
					<div id="rt-breadcrumbs">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('breadcrumb','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Breadcrumbs **/ endif; ?>
					<?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
					<div id="rt-utility">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('utility','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Utility **/ endif; ?>
					<?php /** Begin Expanded Top **/ if ($gantry->countModules('expandedtop')) : ?>
					<div id="rt-expandedtop">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('expandedtop','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Expanded Top **/ endif; ?>
					<?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
					<div id="rt-maintop">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('maintop','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Main Top **/ endif; ?>
					<?php /** Begin Expanded Bottom **/ if ($gantry->countModules('expandedbottom')) : ?>
					<div id="rt-expandedbottom">
						<div class="rt-bg-overlay">
							<div class="rt-container">
								<div class="rt-flex-container">
									<?php echo $gantry->displayModules('expandedbottom','standard','standard'); ?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
					<?php /** End Expanded Bottom **/ endif; ?>
					<?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
					<div id="rt-mainbottom">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Main Bottom **/ endif; ?>
					<?php /** Begin AdditionTop **/ if ($gantry->countModules('additiontop')) : ?>
					<div id="rt-additiontop">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('additiontop','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End AdditionTop **/ endif; ?>
					<?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
					<div id="rt-extension">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('extension','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Extension **/ endif; ?>
					<?php /** Begin AdditionBottom**/ if ($gantry->countModules('additionbottom')) : ?>
					<div id="rt-additionbottom">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('additionbottom','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End AdditionBottom**/ endif; ?>
				</section>
				<?php /** End Main Section **/ endif; ?>

				<?php /** Begin Main Body **/ ?>
				<div id="rt-mainbody-surround">
					<div class="rt-container">
						<?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
					</div>
				</div>
				<?php /** End Main Body **/ ?>

				<?php /** Begin FullWidthBottom **/ if ($gantry->countModules('fullwidthbottom')) : ?>
				<div id="rt-fullwidthbottom">
					<?php echo $gantry->displayModules('fullwidthbottom','basic','standard'); ?>
					<div class="clear"></div>
				</div>
				<?php /** End FullWidthBottom **/ endif; ?>

				<?php /** Begin Footer Section **/ if ($gantry->countModules('bottom') or $gantry->countModules('footer') or $gantry->countModules('copyright')) : ?>
				<footer id="rt-footer-surround">
					<?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
					<div id="rt-bottom">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('bottom','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Bottom **/ endif; ?>
					<?php /** Begin Footer **/ if ($gantry->countModules('footer')) : ?>
					<div id="rt-footer">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('footer','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Footer **/ endif; ?>
					<?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
					<div id="rt-copyright">
						<div class="rt-container">
							<div class="rt-flex-container">
								<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php /** End Copyright **/ endif; ?>
				</footer>
				<?php /** End Footer Surround **/ endif; ?>

			</div>
		</div>

		<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
		<div id="rt-debug">
			<div class="rt-container">
				<div class="rt-flex-container">
					<?php echo $gantry->displayModules('debug','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<?php /** End Debug **/ endif; ?>

		<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
		<?php echo $gantry->displayModules('analytics','basic','basic'); ?>
		<?php /** End Analytics **/ endif; ?>

		<?php /** Popup Login and Popup Module **/ ?>
		<?php echo $gantry->displayModules('login','login','popup'); ?>
		<?php echo $gantry->displayModules('popup','popup','popup'); ?>
		<?php /** End Popup Login and Popup Module **/ ?>
	</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60515929-1', 'auto');
  ga('send', 'pageview');

</script>
</div>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '5tbH2pPkLM';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter28909560 = new Ya.Metrika({id:28909560,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28909560" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '619771568165010',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=HbZjI7rQ50GFEjoZpfw0foFz3hjsCr*drOaPPxiEPdeEgDYoj7al4zi*dfxu90mfYnIUUAfigOzUcYWomW0pK3ZwqgWQRqKQaPt4z64tc9oGd*q2f*cszqChYx2yKFMfbvuDRqxGKWbIGiUPmAhwe2w2EOKhjMgqlyEBFCQCZPA-';</script>
</body>
</html>
<?php
$gantry->finalize();
?>
